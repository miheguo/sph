//引入插件
import Vue from "vue"
import VueRouter from "vue-router"
//使用插件
Vue.use(VueRouter)

//引入路由组件
import Home from '@/pages/Home'
import Search from '@/pages/Search'
import Login from '@/pages/Login'
import Register from '@/pages/Register'
import Detail from '@/pages/Detail'
import AddCartSuccess from '@/pages/AddCartSuccess'
import ShopCart from '@/pages/ShopCart'
import Trade from "@/pages/Trade"
import Pay from "@/pages/Pay"
import PaySuccess from "@/pages/PaySuccess"
import Center from "@/pages/Center"
import store from '@/store'
import MyOrder from '@/pages/Center/myOrder'
import GroupOrder from '@/pages/Center/groupOrder'
//重写push/replace
let originPush = VueRouter.prototype.push
let originreplace = VueRouter.prototype.replace

VueRouter.prototype.push = function (location, reslove, reject) {
    if (reslove && reject) {
        originPush.call(this, location, reslove, reject)
    } else {
        originPush.call(this, location, () => { }, () => { })
    }
}

VueRouter.prototype.replace = function (location, reslove, reject) {
    if (reslove && reject) {
        originreplace.call(this, location, reslove, reject)
    } else {
        originreplace.call(this, location, () => { }, () => { })
    }
}

//路由懒加载



//配置路由
let router = new VueRouter({
    routes: [
        {
            path: '/home',
            //路由懒加载写法
            component: ()=>import ('@/pages/Home'),
            meta: { show: true },
        },
        {
            name: 'search',
            path: '/search/:keyword?',
            component: ()=>import ('@/pages/Search'),
            meta: { show: true },
        },
        {
            path: '/login',
            component: ()=>import ('@/pages/Login'),
        },
        {
            path: '/register',
            component:()=>import ('@/pages/Register'), 
        },
        {
            name: 'detail',
            path: '/detail/:skuid?',
            component: ()=>import ('@/pages/Detail'),
        },
        {
            path: '/addcartsuccess',
            name: 'addcartsuccess',
            component: ()=>import ('@/pages/AddCartSuccess'),
        },
        {
            path: '/shopcart',
            name: 'ShopCart',
            component: ()=>import ('@/pages/ShopCart'),
        },
        {
            path: '/trade',
            name: 'trade',
            component: ()=>import ('@/pages/Trade'),
            //独享守卫
            beforeEnter(to,from,next){
                if(from.path =='/shopcart'){
                    next()
                }else{
                    next(false)
                }
            }
        },
        {
            path: '/pay',
            name: 'pay',
            component: ()=>import ('@/pages/Pay'),
            beforeEnter(to,from,next){
                if(from.path=="/trade"){
                    next();
                }else{
                    next(false)
                }
            }
        },
        {
            path: '/paysuccess',
            name: 'paysuccess',
            component: ()=>import ('@/pages/PaySuccess'),
            beforeEnter(to,from,next){
                if(from.path=="/pay"){
                    next();
                }else{
                    next(false)
                }
            }
        },
        {
            path: '/center',
            name: 'center',
            component: ()=>import ('@/pages/Center'),
            children: [
                {
                    path: 'grouporder',
                    component: GroupOrder,
                },
                {
                    path: 'myorder',
                    component: MyOrder,
                },
                {
                    path:'/center',
                    redirect:'/center/myorder',
                }
            ]
        },
        {
            path: '*',
            redirect: '/home'
        }
    ],
    scrollBehavior(to, from, savedPosition) {
        return { y: 0 };
    },
})
router.beforeEach(async (to, from, next) => {
    //to可以获取到你要跳到那个路由的信息
    //from可以获取到你从那个路由而来的信息
    //next放行函数
    //和视频写的不一样，看91集
    // if (localStorage.getItem("TOKEN")) {
    //     if (to.path == '/login') {
    //         next('/home')
    //     } else {
    //         next()
    //     }
    // } else {

    //     next()
    // }
    // console.log(store);
    let token = store.state.user.token
    let name = store.state.user.name
    if (token) {
        if (to.path == '/login' || to.path == '/register') {
            next('/home')
        } else {
            if (name) {
                next()
            } else {
                try {
                    await store.dispatch('userInfo')
                    next()
                } catch (error) {
                    await store.dispatch('userLogout')
                }

            }
        }
    } else {
        //未登录，不能去交易支付相关，个人中心
        let toPath = to.path
        if(toPath.indexOf('/trade')!=-1 ||toPath.indexOf('pay')!=-1||toPath.indexOf('center')!=-1){
            //把未登录的时候想去而没去成的信息，存储在地址栏中
            next('/login?redirect='+toPath)
        }
        next()
    }

})




export default router;