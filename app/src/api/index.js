//API统一管理
import requests from './request'
import mockRequest from './mockAjax'

//三级联动接口
///api/product/getBaseCategoryList get 无参数

export const reqCategoryList = () => {
    return requests({
        url: '/product/getBaseCategoryList',
        method: 'get'
    })
}
//搜索
export const reqGetSearchInfo = (params) => {
    return requests({
        url: '/list',
        method: 'post',
        data: params
    })
}
//详情页面
export const reqGetDetail = (skuid) => {
    return requests({
        url: `/item/${skuid}`,
        method: 'get',

    })
}

//将产品添加购物车中（获取更新某一个产品的个数）
export const reqAddUpdateShopCart = (skuid, skuNum) => {
    return requests({
        url: `/cart/addToCart/${skuid}/${skuNum}`,
        method: "post"
    })
}

//获取购物车列表数据
export const reqCarList = () => {
    return requests({
        url: '/cart/cartList',
        method: "get"
    })
}
//删除购物车
export const reqDeleteCartByid = (skuId)=>{
    return requests({
        url:`/cart/deleteCart/${skuId}`,
        method:'delete'
    })
}

//修改商品的选择状态
export const reqUpdateCheckedById=(skuId,isChecked)=>{
    return requests({
        url:`/cart/checkCart/${skuId}/${isChecked}`,
        method:'get'
    })
}
//获取验证码
export const  reqGetCode=(phone)=>{
    return requests({
        url:`/user/passport/sendCode/${phone}`,
        method:'get'
    })
}
//注册
export const reqUserRegister=(data)=>{
    return requests({
        url:'/user/passport/register',
        data,
        method:'post'
    })
}
//登录
export const reqUserLogin = (data)=>{
    return requests({
        url:'/user/passport/login',
        data,
        method:'post'
    })
}
//获取用户信息
export const reqUserInfo = ()=>{
    return requests({
        url:'user/passport/auth/getUserInfo',
        method:'get'
    })
}

//退出登录
export const reqLogout = ()=>{
    return requests({
        url:"/user/passport/logout",
        method:'get'
    })
}
//获取用户地址信息
export const reqAddressInfo=()=>{
    return requests({
        url:"/user/userAddress/auth/findUserAddressList",
        method:'get'
    })
}

//获取用户的商品清单
export const reqOrderInfo =()=>{
    return requests({
        url:"/order/auth/trade",
        method:'get'
    })
}
//提交订单
export const reqSubmitOrder =(tradeNo,data)=>{
    return requests({
        url:`/order/auth/submitOrder?tradeNo=${tradeNo}`,
        method:'post',
        data
    })
}
//获取支付信息
export const reqPayInfo =(orderId)=>{
    return requests({
        url:`/payment/weixin/createNative/${orderId}`,
        method:'get'
    })
}
//获取支付订单状态
export const reqPayStatus = (orderId)=>{
    return requests({
        url:`/api/payment/weixin/queryPayStatus/${orderId}`,
        method:"get"
    })
}
//获取个人中心的数据
export const reqMyOrderList = (page,limit)=>{
    return requests({
        url:`/order/auth/${page}/${limit}`,
        method:'get',
    })
}
export const reqGetBannerList = () => mockRequest.get('/banner')
export const reqGetFloorList = () => mockRequest.get('/floor')

