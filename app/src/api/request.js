import axios from "axios"
//引入进度条
import nprogress from "nprogress"
//引入进度条样式
import 'nprogress/nprogress.css'
import store  from '@/store'

const request = axios.create({
    baseURL:'/api',
    timeout:5000,
})

//请求拦截器：在发生请求之前，请求拦截器可以检测到，在请求发送出去之前做一些事情
request.interceptors.request.use((config)=>{
    if(store.state.detail.uuid || localStorage.getItem("TOKEN")){
        config.headers.userTempId = store.state.detail.uuid  || localStorage.getItem("TOKEN")
    }
    if(store.state.user.token){
        config.headers.token= store.state.user.token
    }
    //进度条开始
    nprogress.start()
    return config
})

//响应拦截器
request.interceptors.response.use((res)=>{
    //进度条结束
    nprogress.done()
    //成功回调函数，服务器相应数据回来以后，响应拦截器可以检测到，并且可以做一些事情
    return res.data
    
    
},(err)=>{
    //响应失败回调
    return Promise.reject(new err('false'))
})


export default request