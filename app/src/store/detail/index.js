

import { reqGetDetail,reqAddUpdateShopCart} from "@/api"
//生产临时游客
import {getUUID} from '@/utils/uuid_token'
const actions ={

    async goDetailId({commit},id){
        let result = await reqGetDetail(id)
        // console.log(result);
        if(result.code == 200){
            commit('GODETAILID',result.data)
           }
    },
    async addOrUpdateShopCart({commit},{skuid,skuNum}){
        let result =await reqAddUpdateShopCart(skuid,skuNum)
        //服务器并没有返回数据，不用存数据
        //当前函数会返回一个promise
        if(result.code==200){
            return "ok"
        }else{
            return Promise.reject(new Error('faile'))
        }
    }
}

const mutations={

    GODETAILID(state,goodInfo){
        state.goodInfo = goodInfo
    }
}

const state ={ 
    goodInfo:[],
    uuid:getUUID()

}
//为了简化数据而生，简化仓库的数组
const getters ={
    categoryView(state){
        return state.goodInfo.categoryView || {}
    },
    skuInfo(state){
        return state.goodInfo.skuInfo || {}
    },
    spuSaleAttrList(){
        return state.goodInfo.spuSaleAttrList|| []
    }
    
}

export default {
    namespaced:true,
    state,
    mutations,
    actions,
    getters
}