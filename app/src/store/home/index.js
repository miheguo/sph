import {reqCategoryList} from '@/api'
import {reqGetBannerList,reqGetFloorList} from '@/api/'

const actions ={
  async categorylist({commit}){
        let result = await reqCategoryList()
        // console.log(result)
        if(result.code ==200){
            commit('CATEGORYLIST',result.data)
        }
    },
   async getBannerList({commit}){
        let result = await reqGetBannerList()
        // console.log(result);
        if(result.code ==200){
            commit('GETFBANNERLIST',result.dataa)
        }
    },
  async  getFloorList({commit}){
        let result = await reqGetFloorList()
        if(result.code ==200){
            commit('GETFLOORLIST',result.dataa)
        }
    },
}

const mutations={
    CATEGORYLIST(state,categoryList){
        state.categoryList = categoryList
    },
    GETFBANNERLIST(state,bannerList){
        state.bannerList =bannerList
    },
    GETFLOORLIST(state,floorList){
        state.floorList=floorList
    },
}

const state ={ 
    categoryList:[],
    bannerList:[],
    floorList:[]
}

const getters ={

}

export default {
    namespaced:true,
    state,
    mutations,
    actions,
    getters
}