

import { reqGetSearchInfo,reqGetDetail} from "@/api"

const actions ={
   async getSearchList({commit},params={}){
   let result =  await reqGetSearchInfo(params)
   if(result.code == 200){
    commit('GETSEARCHLIST',result.data)
   }
    },
    async goDetailId({commit},id){
        let result = await reqGetDetail(id)
        console.log(result);
        if(result.code == 200){
            commit('GODETAILID',result.data)
           }
    }
}

const mutations={
    GETSEARCHLIST(state,SearchList){
        state.SearchList = SearchList
    },
    GODETAILID(state,goodDetailList){
        state.goodDetailList = goodDetailList
    }
}

const state ={ 
    goodDetailList:[],
    SearchList:[],
}
//为了简化数据而生，简化仓库的数组
const getters ={
 goodsList(state){
    return state.SearchList.goodsList || []
 },
 trademarkList(state){
    return state.SearchList.trademarkList || []
 },
 attrsList(state){
    return state.SearchList.attrsList || []
 }
}

export default {
    namespaced:true,
    state,
    mutations,
    actions,
    getters
}