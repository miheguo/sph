
import {reqAddressInfo,reqOrderInfo} from "@/api"
const actions = {
    //获取用户地址
   async getUserAddress({commit}){
    let result = await reqAddressInfo()
    if (result.code == 200) {
        commit("GETUSERADDRESS", result.data)
        return 'ok'
    } else {
        return Promise.reject(new Error("faile"))
    }
   },
   //获取商品清单
 async  getOrderInfo({commit}){
    let result = await reqOrderInfo()
    if (result.code == 200) {
        commit("GETUORDERINFO", result.data)
        return 'ok'
    } else {
        return Promise.reject(new Error("faile"))
    }
   }
}
const mutations = {
    GETUSERADDRESS(state,address){
        state.addressInfo=address
    },
    GETUORDERINFO(state,orderInfo){
        state.orderInfo=orderInfo
    }
}
const state = {
    addressInfo:'',
    orderInfo:'',
}
const getters = {

}

export default {
    namespaced: true,
    actions,
    mutations,
    state,
    getters
}