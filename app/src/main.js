import Vue from 'vue'
import App from './App.vue'
//三级联动组件注册全局组件
import TypeNav from '@/components/TypeNav'
//轮播图
import Carousel from '@/components/Carousel'
//分页器
import Pagination from '@/components/Pagination'
Vue.component(TypeNav.name,TypeNav)
Vue.component(Carousel.name,Carousel)
Vue.component(Pagination.name,Pagination)
Vue.config.productionTip = false
//引入路由
import router  from '@/router'
//引入仓库
import store from '@/store'
import "@/mock/mockServe"
//引入轮播图
import 'swiper/css/swiper.css'
// import {reqCategoryList} from '@/api'
// reqCategoryList();

//统一接受api接口的函数
import * as API from '@/api'

//引入饿了么ui
import { Button, MessageBox} from 'element-ui';
Vue.component(Button.name, Button);
//另一种写法挂在原型上
Vue.prototype.$msgbox = MessageBox;
Vue.prototype.$alert = MessageBox.alert;


//懒加载
import VueLazyload from 'vue-lazyload'
import logo from '@/assets/logo.png'
Vue.use(VueLazyload,{
  loading:logo
})


//校验插件,表单验证
import VeeValidate from 'vee-validate'
//中文提示信息
import zh_CN from 'vee-validate/dist/locale/zh_CN'
Vue.use(VeeValidate)
VeeValidate.Validator.localize('zh_CN',{
  messages:{
    ...zh_CN.messages,
    is:(field)=>`${field}必须密码相同`
  },
  attributes:{
    phone:'手机号',
    code:'验证码',
    password:'密码',
    cpassword:'确认密码',
    agree:'协议'
  }
})
//自定义校验规则
VeeValidate.Validator.extend("agree",{
  validate:(value)=>{
    return value
  },
  getMessage:(field)=>field +'必须同意'
})








new Vue({
  render: h => h(App),
  beforeCreate(){
    Vue.prototype.$bus = this
    //api接口放在原型接口上
    Vue.prototype.$API = API
  },
  router,
  store,
}).$mount('#app')
